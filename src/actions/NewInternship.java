package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Query;
import internships.Internship;

/**
 * Servlet implementation class UpdateInternshipDetail
 */
@WebServlet("/NewInternship")
public class NewInternship extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewInternship() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
		Internship update=new Internship();

		update.setAbout(request.getParameter("about"));
		update.setCategory(request.getParameter("category"));
		update.setClosing(new SimpleDateFormat("MM/dd/yyyy").parse(request.getParameter("close")));
		update.setDuration(Integer.parseInt(request.getParameter("duration")));
		update.setEligibility(request.getParameter("eligibility"));
		update.setInformation(request.getParameter("information"));
		update.setOpening(new SimpleDateFormat("MM/dd/yyyy").parse(request.getParameter("open")));
		update.setSeats(Integer.parseInt(request.getParameter("seats")));
		update.setTitle(request.getParameter("title"));
		int new_id=Query.newInternship(update);
		PrintWriter out = response.getWriter();
		
	  if(new_id!=-1)   out.println(new_id);
	  else out.println("Error");
		}
		catch (Exception e){
			PrintWriter out = response.getWriter();
		      out.println("Error");
		      e.printStackTrace();
		}
	}

}
