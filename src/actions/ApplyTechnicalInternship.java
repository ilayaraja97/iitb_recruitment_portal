package actions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.mongodb.Mongo;

import database.Query;
import database.Recruiter;
import exceptions.PageNotFoundException;
import exceptions.UnauthorizedUserException;
import internships.TechnicalApplication;
import mongo.Notification;
import sun.misc.IOUtils;

/**
 * Servlet implementation class ApplyTechnicalInternship
 */
@WebServlet("/ApplyTechnicalInternship")
@MultipartConfig
public class ApplyTechnicalInternship extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplyTechnicalInternship() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
            {
		HttpSession session=request.getSession();
		TechnicalApplication application=new TechnicalApplication();
		application.setAvail(request.getParameter("avail"));
		application.setIntersnhip_id(Integer.parseInt(request.getParameter("internship_id")));
		application.setProf_c(request.getParameter("prof_c"));
		application.setProf_ee(request.getParameter("prof_ee"));
		application.setProf_html(request.getParameter("prof_html"));
		application.setProf_java(request.getParameter("prof_java"));
		application.setProf_db(request.getParameter("prof_db"));
		application.setProf_droid(request.getParameter("prof_droid"));
		application.setProf_js(request.getParameter("prof_js"));
		application.setQuestion1(request.getParameter("question1"));
		application.setQuestion2(request.getParameter("question2"));
		application.setQuestion3(request.getParameter("question3"));
		String range=request.getParameter("range").toString();
		System.out.println(range);
		try {
			application.setRange_start(new SimpleDateFormat("MM/dd/yyyy").parse(range.substring(0, range.indexOf(" "))));
			application.setRange_end(new SimpleDateFormat("MM/dd/yyyy").parse(range.substring(range.indexOf("-")+2,range.length())));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		application.setSkills(request.getParameter("skills"));
		application.setUsername(session.getAttribute("username").toString());
       
                                  
                 InputStream resume = request.getPart("resume").getInputStream();
                 byte[] buffer = new byte[resume.available()];
                 resume.read(buffer);
                 String filePath=getServletContext().getRealPath("/")+"uploads\\";
         	       String fileName=session.getAttribute("username").toString()+".pdf";
                 
                 OutputStream outStream = new FileOutputStream(filePath+fileName);
                 outStream.write(buffer);
                 
               outStream.close();
                 
           
         	   
         	       

         	   try {      System.out.println("Uploaded Filename: " + filePath+fileName );
                    PrintWriter out=response.getWriter();
        	        
               if( Query.submitTechnicalApplication(application))
                 { out.print("OK");
                 Notification user_notify=new Notification();
                 Notification recruiter_notify=new Notification();
                 String title = "",name = "";
               
					title=Query.getIntershipDetail(request.getParameter("internship_id")).getTitle();
					name=Query.getCandidateProfile(session.getAttribute("username").toString()).getName();
					user_notify.setTitle(name+"-"+title);
	                 user_notify.setMessage("You have applied succesfully");
	                 user_notify.setType("ApplicationConfirmation");
	                 recruiter_notify.setTitle(name+"-"+title);
	                 recruiter_notify.setMessage("Has applied");
	                 recruiter_notify.setType("NewApplication");
	                 recruiter_notify.setTimestamp(new Date());
	                ArrayList<Recruiter> panelists = Query.getPanelists();
	                Iterator<Recruiter>panel=panelists.iterator();
	                ArrayList<String>notif=new ArrayList<String>();
	                while(panel.hasNext()){
	                	Recruiter current=panel.next();
	                	
	                	notif.add(current.getUsername());
	                }
	                 mongo.MongoQuery.send_notif(session.getAttribute("username").toString(), "You have applied succesfully", title, "ApplicationConfirmation", new SimpleDateFormat("EEE MMM dd HH:mm:ss 'IST' yyyy").format(new Date()));
	                 mongo.MongoQuery.notify(notif, recruiter_notify);
	                
					
				

                 }
         	  } catch (Exception e) {
					// TODO Auto-generated catch block
         		 PrintWriter out=response.getWriter();
     	        out.print("error");
					e.printStackTrace();
				}
                 
    
}
	
}


