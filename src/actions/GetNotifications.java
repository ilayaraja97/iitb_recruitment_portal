package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import mongo.MongoQuery;
import mongo.Notification;
import utilities.FormatDate;

/**
 * Servlet implementation class GetNotifications
 */
@WebServlet("/GetNotifications")
public class GetNotifications extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetNotifications() {
        super();
        // TODO Auto-generated constructor stub
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession(true);
		ArrayList<Notification>notifications=MongoQuery.getUnreadNotifications(session.getAttribute("username").toString());
        Iterator<Notification> notif_iterator=notifications.iterator();
        JSONObject result=new JSONObject();
        try {
			result.put("count", notifications.size());
		
        String list="";
        while(notif_iterator.hasNext()){
        	Notification current=notif_iterator.next();
        	
        	list+="<li onclick=\"markRead('"+current.getId().toString()+"')\">";
        	if(current.getType().equals("Phonecall"))list+="<h4><a href=''><div class=\"pull-left\" style='margin-right:10px;margin-left:5px'><i class='fa fa-phone text-green' ></i></div></a>";
        	if(current.getType().equals("ApplicationConfirmation"))list+="<h4><a href=''><div class=\"pull-left\" style='margin-right:10px;margin-left:5px'><i class='fa fa-check-circle text-green' ></i></div></a>";
        	if(current.getType().equals("NewApplication"))list+="<h4><a href=''><div class=\"pull-left\" style='margin-right:10px;margin-left:5px'><i class='fa fa-info text-green' ></i></div></a>";
        	
        	list+=""+current.getTitle()+"<small class=\"pull-right\"><i class=\"fa fa-clock-o \" style='margin-right:5px'></i>"+FormatDate.relativeDate(current.getTimestamp())+"</small></h4>";
                     
        		list+="<h6>"+current.getMessage()+"</h6></a></li>";
        		
        		
        	
        }
        
        result.put("list", list);
        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        PrintWriter out=response.getWriter();
        out.print(result.toString());
	}

}
