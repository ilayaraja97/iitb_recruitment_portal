package utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Config {

public static String getUrl(){
	JSONArray config;
	String url="";
	try {
		config = new JSONArray(File.readFile("config.json"));
	
	 JSONObject jsonObject = null;
	
		jsonObject = config.getJSONObject(0);
	

	 url = (String) jsonObject.get("url");
	 
	}
	catch(Exception e){
		e.printStackTrace();
	}
	return url;
}
}
