package utilities;

import java.util.Random;

public class RandomString {
	static final String alphanumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static final String complexAlphanumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
public static String getComplexAlphaNumericString(int length){
	
	return randomString(length,complexAlphanumeric);
	 
	
}

static String randomString( int len ,String charset){
	Random rnd = new Random();
	
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( charset.charAt( rnd.nextInt(charset.length()) ) );
	   return sb.toString();
	}
 
}
