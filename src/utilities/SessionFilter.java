package utilities;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class SessionFilter
 */
@WebFilter("/SessionFilter")
public class SessionFilter implements Filter {

    /**
     * Default constructor. 
     */
    public SessionFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain
	
		
		    HttpServletRequest httpReq = (HttpServletRequest) request;
		    HttpServletResponse httpResp = (HttpServletResponse) response;
		    HttpSession session = httpReq.getSession();
		 
		    httpReq.setCharacterEncoding("UTF-8");
		    httpResp.setCharacterEncoding("UTF-8");
		    httpResp.setContentType("application/json");
		 
		
		    
		    ArrayList<String> Protected=new ArrayList<String>(); //Aray of paged requiring session maintenace
		    Protected.add("/recruiter-home");
		    Protected.add("/candidate-home");
		    Protected.add("/GetNotifications");
		 
		if(session.getAttribute("username")==null)
			{
			if (Protected.contains(httpReq.getServletPath()))
			httpResp.sendRedirect("Error.jsp");
			else chain.doFilter(request, response);
				
			
			}
		
		else	chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
