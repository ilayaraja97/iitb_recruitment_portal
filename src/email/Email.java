/**
 * 
 */
package email;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utilities.File;

/**
 * @author Joey
 * Email Settings defined for gmail
 *
 */
public class Email {
	public static String admin_email;
    private static String admin_password,admin_name;
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Email();
		sendEmail("pintojoey@gmail.com","test","body");

	}
	public static String getAdmin_email(){
		if (Email.admin_name==null)new Email();
		return admin_email;
	}
	public Email(){
		
		try{
			JSONArray config = new JSONArray(File.readFile("config.json"));
		 JSONObject jsonObject = config.getJSONObject(0);

		 admin_email = (String) jsonObject.get("admin_email");
		 admin_password = (String) jsonObject.get("admin_password");
		 admin_name = (String) jsonObject.get("admin_name");
	   
	   }
		catch(JSONException e){
			System.out.println("Incorrect Configuration file syntax");
		}
		
	}
    public static boolean sendEmail(String receiver,String Subject,String MessageText){
    	// Recipient's email ID needs to be mentioned.
    	if (Email.admin_name==null)new Email();
    	String to = receiver;//change accordingly

        // Sender's email ID needs to be mentioned
        String from = admin_name;//change accordingly
        final String username = admin_email;//change accordingly
        final String password = admin_password;//change accordingly

        // Assuming you are sending email through relay.jangosmtp.net
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(username, password);
           }
        });

        try {
           // Create a default MimeMessage object.
           Message message = new MimeMessage(session);

           // Set From: header field of the header.
           message.setFrom(new InternetAddress(from));

           // Set To: header field of the header.
           message.setRecipients(Message.RecipientType.TO,
           InternetAddress.parse(to));

           // Set Subject: header field
           message.setSubject(Subject);

           // Now set the actual message
           message.setText(MessageText);

           // Send message
           Transport.send(message);

           System.out.println("Sent message successfully....");

        } catch (MessagingException e) {
              throw new RuntimeException(e);
        }
        
		return false;
    	
    }
}
