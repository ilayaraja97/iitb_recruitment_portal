package database;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import exceptions.AuthenticationFailedException;
import exceptions.DuplicateKeyException;
import exceptions.PageNotFoundException;
import exceptions.UnauthorizedUserException;
import internships.Application;
import internships.Internship;
import internships.TechnicalApplication;
import useraccounts.Candidate_Profile;
import useraccounts.Candidate_Resume;
import useraccounts.Recruiter_Profile;

public class Query {
	
public Query() throws SQLException {
	
	 new database.Connection();
	
}
public static void main(String[]args){
	new database.Connection();
	try {
		register("pintojoey@gmail.com","joey","candidate","joey","fbargn");
	} catch (DuplicateKeyException e) {
		// TODO Auto-generated catch block
		
		e.printStackTrace();
	}
}
public static boolean register(String username, String password, String usertype, String name,String vercode) throws DuplicateKeyException {
	if (database.Connection.con==null)new database.Connection();
	try {
		
	com.mysql.jdbc.PreparedStatement registration = (PreparedStatement) Connection.con.prepareCall("CALL register (?,?,?,?,?)");
	registration.setString(1, username);

	
	
	MessageDigest md = MessageDigest.getInstance("MD5");
    //Add password bytes to digest
    md.update(password.getBytes());
    //Get the hash's bytes
    byte[] bytes = md.digest();
    //This bytes[] has bytes in decimal format;
    //Convert it to hexadecimal format
    StringBuilder sb = new StringBuilder();
    for(int i=0; i< bytes.length ;i++)
    {
        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
    }
    //Get complete hashed password in hex format
    String encrypt_pass = sb.toString();

	
	registration.setString(2,encrypt_pass);
	registration.setString(3, usertype);
	registration.setString(4, name);
	registration.setString(5, vercode);
	registration.execute();
	
	}
	catch (com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e){
		
		if(e.getSQLState().equals("23000"))
			{e.printStackTrace();
			DuplicateKeyException ex=new DuplicateKeyException("Registration");
		throw ex;
		}
		
	}
	 catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return true;
}
public static String authenticate(String username, String password) throws AuthenticationFailedException {
	String usertype="";
	if (database.Connection.con==null)new database.Connection();
	String encrypt_pass;
	try {
		
	com.mysql.jdbc.PreparedStatement authentication = (PreparedStatement) Connection.con.prepareCall("Select authenticate(?,?) as 'type'");
	authentication.setString(1, username);
	
	
	
	
	MessageDigest md = MessageDigest.getInstance("MD5");
    //Add password bytes to digest
    md.update(password.getBytes());
    //Get the hash's bytes
    byte[] bytes = md.digest();
    //This bytes[] has bytes in decimal format;
    //Convert it to hexadecimal format
    StringBuilder sb = new StringBuilder();
    for(int i=0; i< bytes.length ;i++)
    {
        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
    }
    //Get complete hashed password in hex format
    encrypt_pass = sb.toString();

	
	
	
	authentication.setString(2,encrypt_pass);

	System.out.println(authentication.asSql());
	authentication.execute();
	ResultSet rs=authentication.getResultSet();
	AuthenticationFailedException exception= new AuthenticationFailedException("Login");
	if(rs.next()){
		usertype=rs.getString(1);
		if(!usertype.equals("")){
			
			
		}
		else throw exception;
	}
	else
	{
		
		throw exception;
	
	}
	
	}
	
	catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return usertype;
}
public static ArrayList<Internship>  getInterships(){
	if (database.Connection.con==null)new database.Connection();
	ResultSet rs;
	ArrayList<Internship> list =new ArrayList<Internship> ();
	try {
		com.mysql.jdbc.PreparedStatement getInternships = (PreparedStatement) Connection.con.prepareCall("CALL getInternships()");
		
			rs=getInternships.executeQuery();
			Internship current;
			while(rs.next()){
				current=new Internship();
				current.setId(Integer.parseInt(rs.getString("id")));
				current.setTitle(rs.getString("title"));
				
				current.setCategory(rs.getString("category"));
				current.setOpening(rs.getDate("opening"));
				current.setClosing(rs.getDate("closing"));
				current.setDuration(Integer.parseInt(rs.getString("duration")));
				current.setStatus(rs.getString("status"));
				current.setApplicants(Integer.parseInt(rs.getString("applicants")));
				current.setSeats(Integer.parseInt(rs.getString("seats")));
				current.setHired(Integer.parseInt(rs.getString("hired")));
				list.add(current);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	
	return list;
	
}
public static Internship  getIntershipDetail(String index) throws PageNotFoundException{
	if (database.Connection.con==null)new database.Connection();
	ResultSet rs;
	Internship current = null;
	try {
		com.mysql.jdbc.PreparedStatement getInternshipDetails = (PreparedStatement) Connection.con.prepareCall("CALL getInternshipDetail(?)");
		getInternshipDetails.setString(1,index);
		
			rs=getInternshipDetails.executeQuery();
			
			while(rs.next()){
				current=new Internship();
				current.setId(Integer.valueOf(index));
				current.setTitle(rs.getString("title"));
				current.setCategory(rs.getString("category"));
				current.setOpening(rs.getDate("opening"));
				current.setClosing(rs.getDate("closing"));
				current.setDuration(Integer.parseInt(rs.getString("duration")));
				current.setStatus(rs.getString("status"));
				current.setApplicants(Integer.parseInt(rs.getString("applicants")));
				current.setSeats(Integer.parseInt(rs.getString("seats")));
				current.setHired(Integer.parseInt(rs.getString("hired")));
				current.setAbout(rs.getString("about"));
				current.setEligibility(rs.getString("eligibility"));
				current.setInformation(rs.getString("information"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			PageNotFoundException ex=new PageNotFoundException("internship:"+index);
			throw ex;
		}
		
	
	
	return current;
	
}

public static useraccounts.Recruiter_Profile getRecruiterProfile(String username) throws UnauthorizedUserException{
	if (database.Connection.con==null)new database.Connection();
	ResultSet rs;
	Recruiter_Profile profile = null;
	try {
		com.mysql.jdbc.PreparedStatement profiler = (PreparedStatement) Connection.con.prepareCall("CALL getProfile(?)");
		
		profiler.setString(1, username);
		 rs=profiler.executeQuery();
		 if(rs.next()){
			 profile=new Recruiter_Profile();
			 profile.setUsername(username);
			 profile.setName(rs.getString("name"));
			 profile.setPost(rs.getString("role"));
			 profile.setJoining(rs.getDate("joining_date"));
		 }
		 else {
			 UnauthorizedUserException hacker=new UnauthorizedUserException(username);
				throw hacker;
		 }
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	return profile;
	
}
public static useraccounts.Candidate_Profile getCandidateProfile(String username) throws UnauthorizedUserException{
	if (database.Connection.con==null)new database.Connection();
	ResultSet rs;
	Candidate_Profile profile = null;
	try {
		com.mysql.jdbc.PreparedStatement profiler = (PreparedStatement) Connection.con.prepareCall("CALL getProfile(?)");
		
		profiler.setString(1, username);
		 rs=profiler.executeQuery();
		 if(rs.next()){
			 profile=new Candidate_Profile();
			 profile.setName(rs.getString("name"));
			 profile.setRole(rs.getString("role"));
			 profile.setJoining(rs.getDate("joining_date"));
		 }
		 else {
			 UnauthorizedUserException hacker=new UnauthorizedUserException(username);
				throw hacker;
		 }
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	return profile;
	
}
public static void updateCandidateResume(Candidate_Resume update) throws SQLException  {
	if (database.Connection.con==null)new database.Connection();
	
	
	
		com.mysql.jdbc.PreparedStatement resume = (PreparedStatement) Connection.con.prepareCall("CALL updateResume(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		
		resume.setString(1, update.getUsername());
		resume.setString(2,update.getFirst_name());
		resume.setString(3,update.getLast_name());
		resume.setString(4,update.getContact());
		resume.setString(5,update.getLocation());
		resume.setString(6,update.getInterests());
		resume.setString(7,update.getSummary());//
		resume.setString(8,update.getCourse());
		resume.setString(9,update.getCourseyear());
		resume.setString(10,update.getStream());
		resume.setString(11,update.getUniversity());
		resume.setString(12,update.getGrade());
	System.out.println(resume.asSql());
		resume.execute();
		 
	
	
	
	
}
public static useraccounts.Candidate_Resume getCandidateResume(String username) {
	if (database.Connection.con==null)new database.Connection();
	ResultSet rs;
	Candidate_Resume resume = null;
	try {
		com.mysql.jdbc.PreparedStatement resume_query = (PreparedStatement) Connection.con.prepareCall("CALL getResume(?)");
		
		resume_query.setString(1, username);
		 rs=resume_query.executeQuery();
		 if(rs.next()){
			 resume=new Candidate_Resume();
			 resume.setUsername(rs.getString("email"));
			 resume.setFirst_name(rs.getString("first_name"));
			 resume.setLast_name(rs.getString("last_name"));
			 resume.setContact(rs.getString("contact"));
			 resume.setInterests(rs.getString("interests"));
			 resume.setSummary(rs.getString("summary"));
			 resume.setCourse(rs.getString("course"));
			 resume.setLocation(rs.getString("location"));
			 resume.setCourseyear(rs.getString("courseyear"));
			 
			 resume.setStream(rs.getString("stream"));
			 resume.setUniversity(rs.getString("university"));
			 resume.setGrade(rs.getString("grade"));
		 }
		 else {
			 
		 }
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	return resume;
	
}
public static boolean verify(String username, String vercode) throws SQLException {
	if (database.Connection.con==null)new database.Connection();
	boolean verified=false;
	
		
	com.mysql.jdbc.PreparedStatement verification = (PreparedStatement) Connection.con.prepareCall("Select verify(?,?) as 'verified'");
	verification.setString(1, username);
	verification.setString(2, vercode);
	
	
		verification.execute();
		
	ResultSet rs=verification.getResultSet();
	
	if(rs.next()){
		
		if(rs.getString(1).equals("1")){verified=true;}
		else verified=false;
		return verified;
	}
	else
	{
	    	
		SQLException e=new SQLException();
		throw e;
	}
	

	}
public static boolean updateInternshipDetail(Internship update){

	
	try {
		com.mysql.jdbc.PreparedStatement update_query = (PreparedStatement) Connection.con.prepareCall("CALL `updateInternshipDetail`(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		
		update_query.setInt(1, update.getId());
		update_query.setString(2,update.getTitle());
		update_query.setString(3, update.getCategory());
		java.sql.Date opening = new java.sql.Date(update.getOpening().getTime());
		update_query.setDate(4,opening );
		java.sql.Date closing = new java.sql.Date(update.getClosing().getTime());
		update_query.setDate(5, closing);
		update_query.setInt(6, update.getDuration());
		update_query.setInt(7, update.getSeats());
		update_query.setString(8, update.getAbout());
		update_query.setString(9, update.getEligibility());
		update_query.setString(10, update.getInformation());
		System.out.println("x"+update_query.asSql());
		if(update_query.execute())return true;
		else return false;
		
	} catch (SQLException e) {
		
		e.printStackTrace();
		return false;
	}
		 
	 
	
	
}

public static int newInternship(Internship update){
	if (database.Connection.con==null)new database.Connection();
	
	try {
		com.mysql.jdbc.PreparedStatement insert_query = (PreparedStatement) Connection.con.prepareCall("Select `getNewInternship`(?, ?, ?, ?, ?, ?, ?, ?, ?) as `id`;");
		
		
		insert_query.setString(1,update.getTitle());
		insert_query.setString(2, update.getCategory());
		java.sql.Date opening = new java.sql.Date(update.getOpening().getTime());
		insert_query.setDate(3,opening );
		java.sql.Date closing = new java.sql.Date(update.getClosing().getTime());
		insert_query.setDate(4, closing);
		insert_query.setInt(5, update.getDuration());
		insert_query.setInt(6, update.getSeats());
		insert_query.setString(7, update.getAbout());
		insert_query.setString(8, update.getEligibility());
		insert_query.setString(9, update.getInformation());
		ResultSet rs=insert_query.executeQuery();
		int new_id=-1;
		while(rs.next()){
			new_id=rs.getInt("id");
		}
		return new_id;
	
		
	} catch (SQLException e) {
		
		e.printStackTrace();
			}
	return -1;
		 
	 
	
	
}
public static ArrayList<Recruiter> getPanelists(){
	if (database.Connection.con==null)new database.Connection();
	
	com.mysql.jdbc.PreparedStatement panelists;
	ArrayList<Recruiter>panel_list=new ArrayList<Recruiter>();
	try {
		panelists = (PreparedStatement) Connection.con.prepareCall("CALL getPanelists()");
	
	
	
	
	
		ResultSet rs=panelists.executeQuery();
		while(rs.next()){
			Recruiter current=new Recruiter();
			
				current.setUsername(rs.getString("email"));
				current.setName(rs.getString("name"));
				current.setRole(rs.getString("role"));
			panel_list.add(current);
			
		
	}
	 
}
	 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return panel_list;
}
public static boolean submitTechnicalApplication(TechnicalApplication application){
if (database.Connection.con==null)new database.Connection();
	
	
	
	try {
		PreparedStatement submit_application = (PreparedStatement) Connection.con.prepareCall("CALL `applyTechnicalInternship`(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

		
		submit_application.setInt(1, application.getIntersnhip_id());
		submit_application.setString(2, application.getUsername());
		submit_application.setString(3, application.getProf_c());
		submit_application.setString(4, application.getProf_java());
		submit_application.setString(5, application.getProf_ee());
		submit_application.setString(6, application.getProf_js());
		submit_application.setString(7, application.getProf_html());
		submit_application.setString(8, application.getProf_db());
		submit_application.setString(9, application.getProf_droid());
		submit_application.setString(10, application.getSkills());
		submit_application.setString(11, new SimpleDateFormat("yyyy-MM-dd").format(application.getRange_start()));
		submit_application.setString(12, new SimpleDateFormat("yyyy-MM-dd").format(application.getRange_end()));
		submit_application.setString(13, application.getAvail());
		submit_application.setString(14, application.getQuestion1());
		submit_application.setString(15, application.getQuestion2());
		submit_application.setString(16, application.getQuestion3());
		System.out.println(submit_application.asSql());
	     return submit_application.execute();
		
	}
	catch (Exception e){
		e.printStackTrace();
		return false;
	}
	
}
public static TechnicalApplication getTechnicalApplication(String username,int internship_id){
if (database.Connection.con==null)new database.Connection();
	
	
	try {
		PreparedStatement get_application = (PreparedStatement) Connection.con.prepareCall("CALL `getTechnicalApplication`(?, ?);");
		get_application.setString(2 , username);
		get_application.setInt(1, internship_id);
		ResultSet rs=get_application.executeQuery();
		
		if(!rs.next())return null;
		
		TechnicalApplication application=new TechnicalApplication();
		application.setUsername(username);
		application.setIntersnhip_id(internship_id);
		application.setAvail(rs.getString("available"));
		application.setProf_c(rs.getString("prof_c"));
		application.setProf_db(rs.getString("prof_db"));
		application.setProf_droid(rs.getString("prof_droid"));
		application.setProf_ee(rs.getString("prof_ee"));
		application.setProf_html(rs.getString("prof_html"));
		application.setProf_java(rs.getString("prof_java"));
		application.setProf_js(rs.getString("prof_js"));
		application.setQuestion1(rs.getString("quest_1"));
		application.setQuestion2(rs.getString("quest_2"));
		application.setQuestion3(rs.getString("quest_3"));
		application.setRange_end(rs.getDate("feasible_end"));
		application.setRange_start(rs.getDate("feasible_start"));
		application.setSkills(rs.getString("skills"));
		application.setStatus(rs.getInt("status"));
		application.setAlgo_easy(rs.getInt("algo_easy"));
		application.setAlgo_hard(rs.getInt("algo_hard"));
		application.setAlgo_medium(rs.getInt("algo_medium"));
		application.setJava_basics(rs.getInt("java_basics"));
		application.setJava_oop(rs.getInt("java_oop"));
		application.setDroid_basics(rs.getInt("droid_basics"));
		application.setDroid_projects(rs.getInt("droid_projects"));
		application.setDs_easy(rs.getInt("ds_easy"));
		application.setDs_medium(rs.getInt("ds_medium"));
		application.setDs_hard(rs.getInt("ds_hard"));
		application.setUnderstanding(rs.getInt("understanding"));
		application.setReasoning(rs.getInt("reasoning"));
		application.setCommunication(rs.getInt("communication"));
		
		
		return application;
	}
	catch (Exception e){
		e.printStackTrace();
		return null;
	}
	
	
}
public static ArrayList<Application> getApplications(int status){
if (database.Connection.con==null)new database.Connection();
try {
	PreparedStatement get_application = (PreparedStatement) Connection.con.prepareCall("CALL `getApplications`( ?);");
	get_application.setInt(1, status);
	ResultSet rs=get_application.executeQuery();
	ArrayList<Application> applications=new ArrayList<Application>();
	
	while(rs.next()){
		
		Application current=new Application();
		current.setInternship_id(rs.getInt("internship_id"));
		current.setInternship_name(getIntershipDetail(rs.getString("internship_id")).getTitle());
		current.setApplicant_name(getCandidateProfile(rs.getString("applicant")).getName());
		current.setApplicant_education(rs.getString("courseyear")+","+rs.getString("course"));
		current.setApplicant_college(rs.getString("university"));
		current.setApplicant_email(rs.getString("applicant"));
		current.setApplicant_contact(rs.getString("contact"));
		current.setRound1_comment(rs.getString("round1_comment"));
		current.setRound2_comment(rs.getString("round2_comment"));
		current.setRound2_panelist(rs.getString("round2_panelist"));
		current.setRound2_score(rs.getString("round2_score").equals("")?0:Integer.parseInt(rs.getString("round2_score")));
		current.setRound3_comment(rs.getString("round3_comment"));
		current.setRound3_panelist(rs.getString("round3_panelist"));
		current.setRound3_score(rs.getString("round3_score").equals("")?0:Integer.parseInt(rs.getString("round3_score")));
		current.setRound2_date(rs.getTimestamp("round2_date"));
		current.setRound3_date(rs.getTimestamp("round3_date"));
		applications.add(current);
		
		
		
	}
	return applications;
	
} catch (SQLException | PageNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (UnauthorizedUserException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
return null;
	

	
	
}
public static boolean hold_application(int round,String username, int internship_id,String comment){
try{
	PreparedStatement hold_application = (PreparedStatement) Connection.con.prepareCall("CALL `holdApplication`(?, ?,?,?);");
	hold_application.setInt(1,round);
	hold_application.setString(2 , username);
	hold_application.setInt(3, internship_id);
	hold_application.setString(4, comment);
	
 System.out.println(hold_application.asSql());
	return hold_application.execute();
}
	catch (Exception e){
		e.printStackTrace();
		return false;
	}
	
	
}
public static boolean shortlist_application(int round,String username, int internship_id,String comment){
try{
	PreparedStatement shortlist_application = (PreparedStatement) Connection.con.prepareCall("CALL `shortlistApplication`(?, ?,?,?);");
	shortlist_application.setInt(1,round);
	shortlist_application.setString(2 , username);
	shortlist_application.setInt(3, internship_id);
	shortlist_application.setString(4, comment);
	
 System.out.println(shortlist_application.asSql());
	return shortlist_application.execute();
}
	catch (Exception e){
		e.printStackTrace();
		return false;
	}
	
	
}
public static boolean waitlist_application(int round,String username, int internship_id,String comment){
try{
	PreparedStatement waitlist_application = (PreparedStatement) Connection.con.prepareCall("CALL `waitlistApplication`(?, ?,?,?);");
	waitlist_application.setInt(1,round);
	waitlist_application.setString(2 , username);
	waitlist_application.setInt(3, internship_id);
	waitlist_application.setString(4, comment);
	
 System.out.println(waitlist_application.asSql());
	return waitlist_application.execute();
}
	catch (Exception e){
		e.printStackTrace();
		return false;
	}
	
	
}
public static Application getApplicationDetail(String applicant,int internship_id){
if (database.Connection.con==null)new database.Connection();
try {
	PreparedStatement get_application = (PreparedStatement) Connection.con.prepareCall("CALL `getApplicationDetail`( ?,?);");
	get_application.setString(1, applicant);
	get_application.setInt(2, internship_id);
	ResultSet rs=get_application.executeQuery();
	Application current=new Application();
	
	while(rs.next()){
		
		
		current.setInternship_id(rs.getInt("internship_id"));
		current.setInternship_name(getIntershipDetail(rs.getString("internship_id")).getTitle());
		current.setApplicant_name(getCandidateProfile(rs.getString("applicant")).getName());
		current.setApplicant_education(rs.getString("courseyear")+","+rs.getString("course"));
		current.setApplicant_college(rs.getString("university"));
		current.setApplicant_email(rs.getString("applicant"));
		current.setApplicant_contact(rs.getString("contact"));
		current.setRound1_comment(rs.getString("round1_comment"));
		current.setRound2_comment(rs.getString("round2_comment"));
		current.setRound2_panelist(rs.getString("round2_panelist"));
		current.setRound2_score(rs.getString("round2_score").equals("")?0:Integer.parseInt(rs.getString("round2_score")));
		current.setRound3_comment(rs.getString("round3_comment"));
		current.setRound3_panelist(rs.getString("round3_panelist"));
		current.setRound3_score(rs.getString("round3_score").equals("")?0:Integer.parseInt(rs.getString("round3_score")));
		current.setRound2_date(rs.getTime("round2_date"));
		current.setRound3_date(rs.getTime("round3_date"));
		
	}
	return current;
	
} catch (SQLException | PageNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (UnauthorizedUserException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
return null;
	

	
	
}
public static int getCandidateCount(){
	if (database.Connection.con==null)new database.Connection();
	try {
		PreparedStatement count = (PreparedStatement) Connection.con.prepareCall("CALL getCandidateCount()");
		ResultSet rs=count.executeQuery();
		while(rs.next()){
			return rs.getInt("count");
			
		
	}
	 
}
	 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
}
public static boolean updateTechnicalScreeningScore(TechnicalApplication application){
	if (database.Connection.con==null)new database.Connection();
	try {
		PreparedStatement update = (PreparedStatement) Connection.con.prepareCall("CALL `updateTechnicalScreeningScore`(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		update.setString(1,application.getUsername());
		update.setInt(2,application.getIntersnhip_id());
		update.setInt(3,application.getAlgo_easy());
		update.setInt(4,application.getAlgo_medium());
		update.setInt(5,application.getAlgo_hard());
		update.setInt(6,application.getDs_easy());
		update.setInt(7,application.getDs_medium());
		update.setInt(8,application.getDs_hard());
		update.setInt(9,application.getJava_basics());
		update.setInt(10,application.getJava_web());
		update.setInt(11,application.getJava_oop());
		update.setInt(12,application.getDroid_basics());
		update.setInt(13,application.getDroid_projects());
		update.setInt(14,application.getUnderstanding());
		update.setInt(15,application.getReasoning());
		update.setInt(16,application.getCommunication());
		update.setInt(17,application.getRound2_score());
		
		System.out.println(update.asSql());
		return update.execute();

			
		
	
	 
}
	 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return false;
}
}