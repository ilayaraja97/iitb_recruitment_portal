package useraccounts;

import java.util.Date;

public class Candidate_Profile {
private String name,role;
private java.util.Date joining;
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}



/**
 * @return the joining
 */
public Date getJoining() {
	return joining;
}

/**
 * @param date the joining to set
 */
public void setJoining(java.util.Date date) {
	this.joining = date;
}

/**
 * @return the role
 */
public String getRole() {
	return role;
}

/**
 * @param role the role to set
 */
public void setRole(String role) {
	this.role = role;
}



}
