package useraccounts;

public class Candidate_Resume {
private String  username, first_name, last_name, contact, location, interests, summary, course, courseyear,  stream, university, grade;

/**
 * @return the username
 */
public String getUsername() {
	return username;
}

/**
 * @param username the username to set
 */
public void setUsername(String username) {
	this.username = username;
}

/**
 * @return the first_name
 */
public String getFirst_name() {
	return first_name;
}

/**
 * @param first_name the first_name to set
 */
public void setFirst_name(String first_name) {
	this.first_name = first_name;
}

/**
 * @return the last_name
 */
public String getLast_name() {
	return last_name;
}

/**
 * @param last_name the last_name to set
 */
public void setLast_name(String last_name) {
	this.last_name = last_name;
}

/**
 * @return the contact
 */
public String getContact() {
	return contact;
}

/**
 * @param contact the contact to set
 */
public void setContact(String contact) {
	this.contact = contact;
}

/**
 * @return the location
 */
public String getLocation() {
	return location;
}

/**
 * @param location the location to set
 */
public void setLocation(String location) {
	this.location = location;
}

/**
 * @return the interests
 */
public String getInterests() {
	return interests;
}

/**
 * @param interests the interests to set
 */
public void setInterests(String interests) {
	this.interests = interests;
}

/**
 * @return the summary
 */
public String getSummary() {
	return summary;
}

/**
 * @param summary the summary to set
 */
public void setSummary(String summary) {
	this.summary = summary;
}

/**
 * @return the course
 */
public String getCourse() {
	return course;
}

/**
 * @param course the course to set
 */
public void setCourse(String course) {
	this.course = course;
}

/**
 * @return the courseyear
 */
public String getCourseyear() {
	return courseyear;
}

/**
 * @param courseyear the courseyear to set
 */
public void setCourseyear(String courseyear) {
	this.courseyear = courseyear;
}

/**
 * @return the year
 */


/**
 * @return the stream
 */
public String getStream() {
	return stream;
}

/**
 * @param stream the stream to set
 */
public void setStream(String stream) {
	this.stream = stream;
}

/**
 * @return the university
 */
public String getUniversity() {
	return university;
}

/**
 * @param university the university to set
 */
public void setUniversity(String university) {
	this.university = university;
}

/**
 * @return the grade
 */
public String getGrade() {
	return grade;
}

/**
 * @param grade the grade to set
 */
public void setGrade(String grade) {
	this.grade = grade;
}

}
