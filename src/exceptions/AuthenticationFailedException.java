package exceptions;

public class AuthenticationFailedException extends Exception{
String Locale="";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 public AuthenticationFailedException(String Locale)
	    {  super(Locale);
	       this.Locale=Locale;
	       
	    } 
	    public String getLocale()
	    { 
	        return Locale; 
	    } 
}
