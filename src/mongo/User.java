package mongo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class User {
private String username;
private String type;
private String role;

private ArrayList<String> notifications,unread;

public User(JSONObject jObj){
	try {
		this.setUsername(jObj.getString("username"));
		this.setType(jObj.getString("type"));
		this.setRole(jObj.getString("string"));
		
	JSONArray notification_array=	jObj.getJSONArray("notifications");
	for (int i = 0; notification_array.length() > i; i++) {

		// Extracting json object from particular index of array
		String notification = notification_array.getString(i);
	

		notifications.add(notification);
	}
	JSONArray unread_array=	jObj.getJSONArray("unread");
	for (int i = 0; unread_array.length() > i; i++) {

		// Extracting json object from particular index of array
		String notification = unread_array.getString(i);
	

		unread.add(notification);
	}	
	
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
/**
 * @return the username
 */
public String getUsername() {
	return username;
}
/**
 * @param username the username to set
 */
public void setUsername(String username) {
	this.username = username;
}
/**
 * @return the notifications
 */
public ArrayList<String> getNotifications() {
	return notifications;
}
/**
 * @param jsonArray the notifications to set
 */
public void setNotifications(ArrayList<String> jsonArray) {
	this.notifications = jsonArray;
}
/**
 * @return the unread
 */
public ArrayList<String> getUnread() {
	return unread;
}
/**
 * @param unread the unread to set
 */
public void setUnread(ArrayList<String> unread) {
	this.unread = unread;
}
/**
 * @return the role
 */
public String getRole() {
	return role;
}
/**
 * @param role the role to set
 */
public void setRole(String role) {
	this.role = role;
}
/**
 * @return the type
 */
public String getType() {
	return type;
}
/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}

}
