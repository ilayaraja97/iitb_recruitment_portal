package mongo;

import java.util.ArrayList;
import java.util.Date;

import org.bson.types.ObjectId;



public class Event {
private ObjectId id;
private String title,type,url,detail;
private Date start,end;
private boolean allday;
private ArrayList<String>candidate_actors,recruiter_actors;
/**
 * @return the id
 */
public ObjectId getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(ObjectId id) {
	this.id = id;
}
/**
 * @return the title
 */
public String getTitle() {
	return title;
}
/**
 * @param title the title to set
 */
public void setTitle(String title) {
	this.title = title;
}
/**
 * @return the type
 */
public String getType() {
	return type;
}
/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}
/**
 * @return the url
 */
public String getUrl() {
	return url;
}
/**
 * @param url the url to set
 */
public void setUrl(String url) {
	this.url = url;
}
/**
 * @return the detail
 */
public String getDetail() {
	return detail;
}
/**
 * @param detail the detail to set
 */
public void setDetail(String detail) {
	this.detail = detail;
}
/**
 * @return the start
 */
public Date getStart() {
	return start;
}
/**
 * @param start the start to set
 */
public void setStart(Date start) {
	this.start = start;
}
/**
 * @return the end
 */
public Date getEnd() {
	return end;
}
/**
 * @param end the end to set
 */
public void setEnd(Date end) {
	this.end = end;
}
/**
 * @return the candidate_actors
 */
public ArrayList<String> getCandidate_actors() {
	return candidate_actors;
}
/**
 * @param candidate_actors the candidate_actors to set
 */
public void setCandidate_actors(ArrayList<String> candidate_actors) {
	this.candidate_actors = candidate_actors;
}
/**
 * @return the recruiter_actors
 */
public ArrayList<String> getRecruiter_actors() {
	return recruiter_actors;
}
/**
 * @param recruiter_actors the recruiter_actors to set
 */
public void setRecruiter_actors(ArrayList<String> recruiter_actors) {
	this.recruiter_actors = recruiter_actors;
}
/**
 * @return the allday
 */
public boolean isAllday() {
	return allday;
}
/**
 * @param allday the allday to set
 */
public void setAllday(boolean allday) {
	this.allday = allday;
}

}
