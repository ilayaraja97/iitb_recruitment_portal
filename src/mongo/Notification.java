package mongo;

import java.util.Date;

import org.bson.types.ObjectId;

public class Notification {
private String message,title,type;
private Date timestamp;
private ObjectId id;
/**
 * @return the timestamp
 */
public Date getTimestamp() {
	return timestamp;
}
/**
 * @param timestamp the timestamp to set
 */
public void setTimestamp(Date timestamp) {
	this.timestamp = timestamp;
}
/**
 * @return the message
 */
public String getMessage() {
	return message;
}
/**
 * @param message the message to set
 */
public void setMessage(String message) {
	this.message = message;
}
/**
 * @return the type
 */
public String getType() {
	return type;
}
/**
 * @param type the type to set
 */
public void setType(String type) {
	this.type = type;
}
/**
 * @return the id
 */
public ObjectId getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(ObjectId id) {
	this.id = id;
}
/**
 * @return the title
 */
public String getTitle() {
	return title;
}
/**
 * @param title the title to set
 */
public void setTitle(String title) {
	this.title = title;
}
}
