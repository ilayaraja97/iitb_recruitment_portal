package mongo;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import utilities.FormatDate;

public class MongoQuery {
	static MongoClient mongoClient;
	DB db;
	static DBCollection users;
	static DBCollection notifications;
	static DBCollection events;
public static void main(String[]args){
//	 mongo.MongoQuery.send_notif("pintojoey@gmail.com", "You have applied succesfully", "test", "ApplicationConfirmation", new SimpleDateFormat("EEE MMM dd HH:mm:ss 'IST' yyyy").format(new Date()));
	newUser("pintojoey@gmail.com");
	newUser("pintojoe@gmail.com");
  Notification test=new Notification();
  test.setTitle("Test");
  test.setMessage("success");
  test.setType("test");
  test.setTimestamp(new Date());
  ArrayList<String> Test_users=new ArrayList<String>();
  Test_users.add("pintojoey@gmail.com");
  Test_users.add("pintojoe@gmail.com");
  notify(Test_users, test);

//	notify("pintojoe@gmail.com","Notification 6","Registration","12-13-2015 21:01:30");
//	notify("pintojoey@gmail.com","Hi there unreadmessage","Test","05-12-2015 12:28:30");
//	int count=getAllNotifications("pintojoe@gmail.com").size();
//	System.out.println(count);	
//	ObjectId del=new ObjectId("566d5b5bcd81968e6dc0d75f");
//	markAsRead("pintojoe@gmail.com", del);

//	Event test=new Event();
//	test.setTitle("Happy Birthday");
//	ArrayList<String> candidates = new ArrayList<String>();
//	candidates.add("candidate1");
//	ArrayList<String> recruiters = new ArrayList<String>();
//	recruiters.add("recruiter1");
//	test.setRecruiter_actors(recruiters);
//	test.setDetail("Detail");
//	test.setAllday(true);
//	//test.setEnd(null);
//	test.setCandidate_actors(candidates);
//	test.setStart(new Date());
//	test.setType("Birthday");
//	test.setUrl("Url");
//	addEvent(test);
//	Event test2=new Event();
//	test2.setTitle("Happy Birthday2");
//	ArrayList<String> candidates2 = new ArrayList<String>();
//	candidates2.add("candidate2");
//	ArrayList<String> recruiters2 = new ArrayList<String>();
//	recruiters2.add("recruiter2");
//	test2.setRecruiter_actors(recruiters2);
//	test2.setDetail("Detail2");
//	test.setAllday(false);
//	test2.setCandidate_actors(candidates2);
//	test2.setStart(new Date());
//	test2.setType("Birthday2");
//	test2.setUrl("Url2");
//	addEvent(test2);
//	
//	for(Event object: getAllEvents()){
//		  System.out.println(object.getTitle());
//	} 
}
public MongoQuery(){
	try{
		mongoClient = new MongoClient( "localhost" , 27017 );
		db = mongoClient.getDB("iitb");
		users = db.getCollection("users");
		notifications = db.getCollection("notifications");
		events=db.getCollection("events");
	}
	catch (UnknownHostException e){
		e.printStackTrace();
		
	}
}
public static boolean newUser(String username) {
	if(mongoClient==null)new MongoQuery();
	
	DBObject new_user_query = BasicDBObjectBuilder.start()
			.add("username", username)
			.add("notifications",new ArrayList<ObjectId>())
			.add("unread",new ArrayList<ObjectId>())
			.add("conversations",new ArrayList<ObjectId>())
			.get();
	//
    WriteResult cursor = users.insert(new_user_query);
    System.out.println(cursor.toString());
	return true;
}

public static boolean notify(ArrayList<String> user_list,Notification notif){
try	{	if(mongoClient==null)new MongoQuery();
	//db.users.update({"username":"joey"},{$push:{"notifications":"1"}})
System.out.println("reached");	
DBObject new_notification_query = BasicDBObjectBuilder.start()
			.add("message", notif.getMessage())
			.add("title",notif.getTitle())
			.add("type",notif.getType())
			.add("timestamp",new SimpleDateFormat("EEE MMM dd HH:mm:ss 'IST' yyyy").format(notif.getTimestamp()))
			.get();
	
	 notifications.insert(new_notification_query);
	ObjectId newnotif =(ObjectId)new_notification_query.get("_id");
	
	Iterator<String> user_iterator=user_list.iterator();
	System.out.println(user_list.size()+"XX");
	while(user_iterator.hasNext()){
		String username=user_iterator.next();
		DBObject get_user = BasicDBObjectBuilder.start().add("username", username).get();
		System.out.println(username+"XX");

		DBObject notificationlist = new BasicDBObject("notifications", newnotif).append("unread", newnotif);
			DBObject add_notification_query = new BasicDBObject("$push", notificationlist);
		
		WriteResult cursor=users.update(get_user, add_notification_query);
		cursor.toString();
	}

	return true;
}catch(Exception e){
	e.printStackTrace();
	return false;}
}

public static boolean send_notif(String username,String Message,String Title,String Type,String Timestamp) {
	if(mongoClient==null)new MongoQuery();
	
	DBObject new_notification_query = BasicDBObjectBuilder.start()
			.add("message", Message)
			.add("type",Type)
			.add("title",Title)
			.add("timestamp",Timestamp)
			.get();
	
	 notifications.insert(new_notification_query);
	ObjectId newnotif =(ObjectId)new_notification_query.get("_id");
//	 System.out.println(newnotif.toString());
	
		DBObject get_user = BasicDBObjectBuilder.start().add("username", username).get();
		

	DBObject notificationlist = new BasicDBObject("notifications", newnotif).append("unread", newnotif);
		DBObject add_notification_query = new BasicDBObject("$push", notificationlist);
	
	WriteResult cursor=users.update(get_user, add_notification_query);
	cursor.toString();
	
    //System.out.println(cursor.isUpdateOfExisting());
	  
	return true;
}


public static ArrayList<Notification> getUnreadNotifications(String username){
	if(mongoClient==null)new MongoQuery();
ArrayList<Notification> notification_list=new ArrayList<Notification>();
	DBObject note=new BasicDBObject("$unwind","$unread");
	DBObject match=new BasicDBObject("$match",new BasicDBObject("username",username));
	DBObject sort=new BasicDBObject("$sort",new BasicDBObject("_id",-1));
	BasicDBObject project_param=new BasicDBObject(new BasicDBObject("_id","$unread").append("username", 1));
	DBObject project=new BasicDBObject("$project",project_param);
	

	  Iterator<DBObject> notification_iterator = users.aggregate(note,project,match,sort).results().iterator();
	
	while(notification_iterator.hasNext()){
		
	
		DBObject notification=notification_iterator.next();
		DBObject notif_detail = BasicDBObjectBuilder.start().add("_id",notification.get("_id")).get();	
		DBCursor notif_info=notifications.find(notif_detail);
		DBObject current = notif_info.next();
		Notification notif=new Notification();
		notif.setMessage(current.get("message").toString());
		notif.setTitle(current.get("title").toString());
		notif.setId((ObjectId) current.get("_id"));
		notif.setType(current.get("type").toString());
		try {
			
			notif.setTimestamp(FormatDate.parseIST( current.get("timestamp").toString()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(notif.getTimestamp().getTime()<new Date().getTime())	notification_list.add(notif);
	}
	Collections.sort(notification_list,new Comparator<Notification>() { 
        
		@Override
		public int compare(Notification arg0, Notification arg1) {
			// TODO Auto-generated method stub
			if(arg0.getTimestamp().before(arg0.getTimestamp()))
				return -1;
			else	return 0;
		} 
    }); 
	return notification_list;
	}


public static ArrayList<Notification> getAllNotifications(String username){
	if(mongoClient==null)new MongoQuery();
ArrayList<Notification> notification_list=new ArrayList<Notification>();
	DBObject note=new BasicDBObject("$unwind","$notifications");
	DBObject match=new BasicDBObject("$match",new BasicDBObject("username",username));
	DBObject sort=new BasicDBObject("$sort",new BasicDBObject("_id",-1));
	BasicDBObject project_param=new BasicDBObject(new BasicDBObject("_id","$notifications").append("username", 1));
	DBObject project=new BasicDBObject("$project",project_param);
	

	  Iterator<DBObject> notification_iterator = users.aggregate(note,project,match,sort).results().iterator();
	 
	while(notification_iterator.hasNext()){
			
	
		DBObject notification=notification_iterator.next();
		DBObject notif_detail = BasicDBObjectBuilder.start().add("_id",notification.get("_id")).get();	
		DBCursor notif_info=notifications.find(notif_detail);
		DBObject current = notif_info.next();
		Notification notif=new Notification();
		notif.setMessage(current.get("message").toString());
		notif.setTitle(current.get("title").toString());
		notif.setId((ObjectId) current.get("_id"));
		notif.setType(current.get("type").toString());
		try {
			notif.setTimestamp(FormatDate.parseIST( current.get("timestamp").toString()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(notif.getTimestamp().getTime()<new Date().getTime())	notification_list.add(notif);
	}
	Collections.sort(notification_list,new Comparator<Notification>() { 
        
		@Override
		public int compare(Notification arg0, Notification arg1) {
			// TODO Auto-generated method stub
			if(arg0.getTimestamp().before(arg0.getTimestamp()))
				return -1;
			else	return 0;
		} 
    }); 
	return notification_list;
	}

public static void markAsRead(String username,ObjectId notif){
		if(mongoClient==null)new MongoQuery();
		DBObject user_instance = BasicDBObjectBuilder.start().add("username", username).get();
		DBObject pull_unread = BasicDBObjectBuilder.start().add("$pull", new BasicDBObject("unread",notif)).get();
		users.update(user_instance, pull_unread);
		
			
	}
public static void addEvent(Event event){
	if(mongoClient==null)new MongoQuery();
	DBObject event_insert_query=BasicDBObjectBuilder.start()
			.add("title",event.getTitle())
			.add("type", event.getType())
			.add("start",new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(event.getStart()))
			.add("end",event.getEnd()==null?null:new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(event.getEnd()))
			.add("allday",event.isAllday())
			.add("url", event.getUrl())
			.add("detail", event.getDetail())
			.add("candidate", event.getCandidate_actors())
			.add("recruiters", event.getRecruiter_actors())
			.get();
	events.insert(event_insert_query);
	
}
public static ArrayList<Event>  getAllEvents(){
	if(mongoClient==null)new MongoQuery();
	DBObject event_retrieve_query=BasicDBObjectBuilder.start().get();
	DBCursor cursor=events.find(event_retrieve_query);
	ArrayList<Event> EventList=new ArrayList<Event>();
	while(cursor.hasNext()){
		DBObject current_event=cursor.next();
		Event current=new Event();
		current.setId((ObjectId) current_event.get("_id"));
		current.setTitle(current_event.get("title").toString());
		current.setType(current_event.get("type").toString());
		
			try {
				current.setStart(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(current_event.get("start").toString()));
				current.setEnd(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").parse(current_event.get("start").toString()));

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		current.setAllday((boolean) current_event.get("allday"));
		current.setUrl(current_event.get("url").toString());
		current.setDetail(current_event.get("detail").toString());
		BasicDBList candidate_list = (BasicDBList) current_event.get("candidate");
		ArrayList<String>candidates=new ArrayList<String>();
		for (Object user : candidate_list)candidates.add(user.toString());
		current.setCandidate_actors(candidates);
		
		BasicDBList recruiter_list = (BasicDBList) current_event.get("candidate");
		ArrayList<String>recruiters=new ArrayList<String>();
		for (Object user : recruiter_list)recruiters.add(user.toString());
		current.setCandidate_actors(recruiters);
		EventList.add(current);
	}
			
	return EventList;
	
}
}

