<!DOCTYPE html>
<%@page import="database.Query"%>
<%@page import="internships.Internship"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import ="java.sql.*" %>
<%@ page import ="useraccounts.Recruiter_Profile.*" %>
<%@ page import="java.util.ArrayList.*" %>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Recruiter | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
   <link rel="stylesheet" href="plugins/iCheck/all.css">
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
 
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style type="text/css">
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #2571DE;
   
    
}
.form-group{
	margin-top: 30px;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <jsp:include page="recruiter-sidebar.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      
    <section class="content col-lg-6">
      
          <div class="box box-warning ">
              
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                 
                <div class="form-group">
                  <label>
                 Proficiency in C/Python
                </label>
                <div>
                 <input type="radio" name="c" class="flat-red" checked>
                   Unaware
                 <input type="radio" name="c" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="c" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="c" class="flat-green" >
                 Expert
                
                </div>
              </div>

                <div class="form-group">
                <label>
                 Proficiency in Java
                </label>
                <div>
                 <input type="radio" name="java" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="java" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="java" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="java" class="flat-green" >
                 Expert
                
                </div>
              </div>
                <div class="form-group">
                <label>
                 Proficiency in Servlets and JSP (Java EE)
                </label>
                <div>
                 <input type="radio" name="javaEE" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="javaEE" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="javaEE" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="javaEE" class="flat-green" >
                 Expert
                
                </div>
              </div>
                <div class="form-group">
                <label>
                 Proficiency in Ajax and JavaScript
                </label>
                <div>
                 <input type="radio" name="JS" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="JS" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="JS" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="JS" class="flat-green" >
                 Expert
                
                </div>
              </div>
                <div class="form-group">
                <label>
                 Proficiency in HTML,CSS 
                </label>
                <div>
                 <input type="radio" name="html" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="html" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="html" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="html" class="flat-green" >
                 Expert
                
                </div>
              </div>       
                <div class="form-group">
                <label>
                 Proficiency in SQL Database (MySQL/OracleDB)
                </label>
                <div>
                 <input type="radio" name="sql" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="sql" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="sql" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="sql" class="flat-green" >
                 Expert
                
                </div>
              </div>      
               <div class="form-group">
                <label>
                 Proficiency in Android
                </label>
                <div>
                 <input type="radio" name="android" class="flat-red" checked>
                   Unaware
               
                 <input type="radio" name="android" class="flat-yellow">
                  Beginner
                
                 <input type="radio" name="android" class="flat-blue">
                  Intermediate
                            
                  <input type="radio" name="android" class="flat-green" >
                 Expert
                
                </div>
              </div>
             
             <div class="form-group">
                <label>Other Skills</label>
                <select class="form-control select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                  <option>MongoDB</option>
                  <option>Linux</option>
                  <option>Shell Scripting</option>
                  <option>Arduino</option>
                  <option>IOS</option>
                  <option>Matlab</option>
                  <option>Artificial Intelligence</option>
                  <option>Wordpress</option>
                  <option>Cloud Computing</option>
                  <option>Distributed computing</option>
                  
                </select>
              </div>
                <label>Feasible Internship Period</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" onchange="update()" class="form-control pull-right" id="range" value="<%                 
               //   out.print(new SimpleDateFormat("MM/dd/yyyy").format(display.getOpening()).toString()+" - "+new SimpleDateFormat("MM/dd/yyyy").format(display.getClosing()).toString()); %>">
                </div>
             
             
             <div class="form-group">
                <label>
                 Available for face to face interview at IIT-Bombay Campus within next week
                </label>
                <div>
                 <input type="radio" name="android" class="flat-red" checked>
                   No &nbsp;&nbsp;
               
                 <input type="radio" name="android" class="flat-yellow">
                  May be &nbsp;&nbsp;
                
                 <input type="radio" name="android" class="flat-green">
                  Yes
                            
                                  
                </div>
              </div>
             <div class="form-group">
                  <label for="exampleInputFile">Upload Resume</label>
                  <input type="file" id="exampleInputFile">

                  <p class="help-block">Upload Your resume in PDF format</p>
                </div>
                
                
             
<!-- Select multiple-->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <section class="content col-lg-6">
      
          <div class="box box-success ">
              
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Benefits of the Internship Programme
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
<ol>                      
<li>Learning and interaction opportunities with IITB professors, students etc. on a wide range of technical and research topics/experiments.</li>
<li>Students will get exposure to coding, software testing, presentations, proposal writing, event management and teamwork.</li>
<li>Stipend will be provided and vary according to the performance and delivery.</li>
<li>LOR (Letter of recommendation - both online (for MS applications etc.) and offline can be provided from IITB professor FOR CANDIDATES PERFORMING EXCEPTIONALLY.</li>
<li>Certificate on IITB letter-head will be provided on successful completion of internship.</li>
<li>Accomodation at IITB hostel is possible but subject to availability.</li>
</ol>
                    </div>
                  </div>
                </div>
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Eligibility
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="box-body">
                       <ol>
                      
<li>ONLY applicants who can work FULL TIME from IIT Bombay campus should apply.
<li>Freshers and Immediate joiners with duration 6 months and above are preferred.
<li>Candidates with a CGPA above 7.5 (on a scale of 10) are preferred.</li>
<li>Applicants are expected to be proficient in Java, along with Android or JSP and Servlets.</li>
<li>Knowledge on Spring, AngularJS, JQuery will add value to your resume.</li>
<li>Good communication, analytical and presentation skills are important.</li>
<li>Applicants currently pursuing or completed B.E / B.Tech / M.Tech in Computer Science / Information Technology. Those pursuing degree need an NOC from college to apply for internship before coming for the interview.</li>
<li>Candidates who have experience on live development/research projects will be preferred.</li>
<li>Knowledge in the field of Machine Learning and/or AI is an added advantage.</li>
<li>Shortlisted candidates (only those pursuing their degrees) MUST provide a Mentor from the parent University who will be responsible for daily feedbacks on candidate's performance.</li>
                       </ol>
                    </div>
                  </div>
                </div>
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        Selection Procedure
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body">
                  <ol>
                  
<li>Submission of application</li>
<li>Telephonic screening of around 15 minutes over Skype/Google Hangouts for shortlisted candidates.</li>
<li>Candidates who clear the screening need to come to IIT Bombay for Face to Face interview process for a day along with NOC from College to allow them to participate in the internship if selected.</li>
<li>Post interview, selected Candidates will receive offer letter which they need to confirm within 3 days keeping the college faculty representative in CC over email.</li>
<li>They need to send soft copies of the necessary documents/emails as mentioned below within 7 working days. Candidates will need to present the hard copies of the documents on the day of their joining.</li>
                  </ol>
                    </div>
                  </div>
                </div>  
                <div class="panel box box-warning">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        Required Documents
                      </a>
                    </h4>
                  </div>
                  <div id="collapseFour" class="panel-collapse collapse">
                    <div class="box-body">
                  <ol>

<li>Letters of Recommendation (LOR) from 2 professors of their college. (Signed And Attested)
<li>E-Mail from their professor to the project manager
<li>Our Internship program requires that the college assigns an internal guide/faculty mentor to whom the project manager will share periodic reports about candidate's performance at IITB. The e-mail should be a confirmation letter from the faculty/professor stating that you will work for the entire duration of the internship as mentioned on your offer letter, and he/she can be contacted for any issues whatsoever in relation to your work here. The email must also share the contact number, and the availability of the internal guide to take feedbacks.</li>
<li>Letter from the candidate ON COLLEGE LETTER HEAD confirming that he/she will work for mentioned duration for the internship at IIT-Bombay. (Signed And Attested with your college seal and signatures)</li>
<li>There is a separate form for external students. Candidates will need to get the Principal/HOD Seal and Signature on the form before they come for the internship.</li>
<li>Their B.tech/B.E marksheets zipped into a single folder named Marksheets/Transcripts for verification of CGPA.</li>
<li>Students will need to print the Internship Undertaking and get it duly signed and stamped by HOD.</li>
                  </ol>
                    </div>
                  </div>
                </div>
              
                <div class="panel box box-grey">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        Important Links
                      </a>
                    </h4>
                  </div>
                  <div id="collapseFive" class="panel-collapse collapse">
                    <div class="box-body">
                  <ol>

<li href="http://ruralict.cse.iitb.ac.in/Downloads/internship/Procedure.pdf">Procedure for doing project work in IITB</li>
                  </ol>
                    </div>
                  </div>
                </div>
              
              
              </div>
            </div>
            
                
                <div class="alert alert-danger alert-dismissible" style="display:none" id="error">
                <i class="icon fa fa-ban"></i> 
              <span id="message"></span> 
              </div>
                <div>
                <button type="button" id="save" class="btn btn-block btn-success"  style="display:" onclick="savechanges()" >Apply</button>
                </div>
				
                <!-- textarea -->



					</form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <!-- /.content -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   </div>
  <jsp:include page="recruiter-footer.jsp"></jsp:include>
    
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>

<!-- AdminLTE for demo purposes -->


<script defer>

var title,category,range,duration,seats,about,eligibility,information;
update();/*
document.getElementById("save").style.display="none"; //hiding save button
duration=document.getElementById('duration').value;
seats=document.getElementById('seats').value;
about=document.getElementById('abouteditor').innerHTML;
eligibility=document.getElementById('eligibilityeditor').innerHTML;
information=document.getElementById('informationeditor').innerHTML;*/

var index="<% //out.print(display.getId());%>";
  function savechanges(){
	  update();
	 closeall();
 	  if(validate()){
			
			 var xmlhttp=new XMLHttpRequest();
			    xmlhttp.onreadystatechange=function() {
			        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			      	
			if (xmlhttp.responseText.indexOf("OK")!=-1){
				document.getElementById("save").style.display="none";
				document.getElementById("error").style.display="none";
				document.getElementById("message").innerHTML="";
			 
			}
			else {
				document.getElementById("error").style.display="";
				document.getElementById("message").innerHTML="There was an error saving changes";
			}


					}
					}
			    xmlhttp.open("POST","UpdateInternshipDetail",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send('index='+index+"&about="+about+"&category="+category+"&close="+close+"&duration="+duration+"&eligibility="+eligibility+"&information="+information+
			    "&open="+open+"&seats="+seats+"&title="+ title);
			    
			    document.getElementById('save').style.display="";
			document.getElementById('error').style.display="none";
		  }
  }
  function update(){
	/*  title=document.getElementById('title').value;
	  category=document.getElementById('category').value;
	  range=document.getElementById('range').value;
	  try{
		  open=range.substring(0,range.indexOf(" "));
		  close=range.substring(range.indexOf("-")+2,range.length);
		  
	  }
	  catch (Exception){
		  open="";
		  close="";
	  }
	  
	  range="";
      document.getElementById('save').style.display="";*/
  }
  function validate(){
	  if(title==""||category=="" || open=="" || close=="" ||duration=="" || seats=="" || about=="" || eligibility=="" || information=="")
	  {
		  document.getElementById('error').style.display="";
		  document.getElementById('message').innerHTML=" Please Fill All Fields";
		  return false;}
	  else {
		  
		  document.getElementById('error').style.display="none";
		  return true;}
  }
  function ckedit(box){
	  
	  closeall();
	  
		var data=document.getElementById(box).innerHTML;
		  document.getElementById(box).innerHTML="";
		  CKEDITOR.replace(box);
		  CKEDITOR.instances[box].setData(data);
		  CKEDITOR.instances[box].on('change', function() {
			  if(box=='abouteditor') about=CKEDITOR.instances[box].getData();
			  if(box=='eligibilityeditor') eligibility=CKEDITOR.instances[box].getData();
			  if(box=='informationeditor') information=CKEDITOR.instances[box].getData();
		  update();
		  });
	  
	  }
  function closeall(){
	  for(name in CKEDITOR.instances)
	  {
		  
		  var data=CKEDITOR.instances[name].getData();
		  
	      CKEDITOR.instances[name].destroy(true);
	      document.getElementById(name).innerHTML=data;
	  }
  }

 
  $(function () {
	  
	 
	  $(".select2").select2({
		  tags: true,
		  tokenSeparators: [',', ' '],
		 
		})

	
    $(' input[type="radio"].flat-green').iCheck({
     
      radioClass: 'iradio_flat-green'
    });
    $(' input[type="radio"].flat-red').iCheck({
        
        radioClass: 'iradio_flat-red'
      });
  $(' input[type="radio"].flat-blue').iCheck({
        
        radioClass: 'iradio_flat-blue'
      });
  $(' input[type="radio"].flat-yellow').iCheck({
      
      radioClass: 'iradio_flat-yellow'
    });
		  
	    /* jQueryKnob */
 		$('#range').daterangepicker();
	    $("#seats").knob({
	     
	       max:10,
	      draw: function () {
	    	  this.max=10;
	    	  
	      },change: function (value) {
	            seats=value.toFixed(0);
	            update();
	        }
	    
	    });
	    $("#duration").knob({
		     
		       max:10,
		      draw: function () {
		    	  this.max=10;
		    	  
		      },change: function (value) {
		            duration=value.toFixed(0);
		            update();
		        }
		    
		    });
	    
	    
	    /* END JQUERY KNOB */
  });
  
  //iCheck for checkbox and radio inputs
 
</script>
<!-- Bootstrap WYSIHTML5 -->

</body>
</html>
