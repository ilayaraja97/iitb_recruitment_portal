<!DOCTYPE html>
<%@page import="java.util.Iterator"%>
<%@page import="internships.Application"%>
<%@page import="sun.security.util.PendingException"%>
<%@ page import="database.Query"%>
<%@ page import="internships.Internship"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import ="java.sql.*" %>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Recruiter | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <!-- Daterange picker -->
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

  <!-- fullCalendar 2.2.5-->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script>
  function detail(id){
	  window.location="internship-detail?index="+id;
  }
  function editdetail(id){
	  window.location="edit-internship?index="+id;
  }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini" data-root="<%out.print(getServletContext().getRealPath("/")+"uploads\\");%>">
<div class="wrapper">

  <jsp:include page="recruiter-sidebar.jsp"></jsp:include>
 <!-- Button trigger modal -->


<!-- Modal -->

<div class="modal fade" id="phonecall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id="schedulephonecall" class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header " style="background-color:green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color:white">Telephonic Screening</h4>
      </div>
      <div class="modal-body">
      <div class="box-body" >
              <!-- the events -->
              <div class="row">
             
             
      <div class="col-lg-12" >
              <!-- the events -->
                <section class="content col-lg-6">
                 <h1><a class="glyphicon glyphicon-phone" href="" id="phonecall_contact"></a></h1>
          
      <h2 style="color:red" id="phonecall_username"></h2>
        <h4><span id="phonecall_email"></span></h4> 
           <button title="Resume" type="button" onclick="window.open(this.href,'popUpWindow','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;" id="phonecall_resume"><i class="fa fa-file-pdf-o" >&nbsp;Resume</i></button>
        
             <h4 ><span id="phonecall_education"></span>&nbsp;<span id="phonecall_university"></span></h4> 
            
          <div class="box box-warning ">
              
            <!-- /.box-header -->
            <div class="box-body">
              
                <!-- text input -->
                 
                <table>
                
                 <tr><th> Proficiency in C/Python</th><th><span id="phonecall_prof_c"></span></tr> 
                 <tr><th> Proficiency in Java</th><th><span id="phonecall_prof_java"></span></tr>
                 <tr><th> Proficiency in Servlets and JSP (Java EE)</th><th><span id="phonecall_prof_ee"></span></tr>
                 <tr><th> Proficiency in Ajax and JavaScript</th><th><span id="phonecall_prof_js"></span></tr>
                 <tr><th> Proficiency in HTML,CSS </th><th><span id="phonecall_prof_html"></span></tr>
                 <tr><th> Proficiency in SQL Database (MySQL/OracleDB)</th><th><span id="phonecall_prof_db"></span></tr>
                 <tr><th> Proficiency in Android</th><th><span id="phonecall_prof_droid"></span></tr>
                 <tr><th> Other Skills</th><th><span id="phonecall_skills"></span></tr>
                 <tr><th> Available for campus interview</th><th><span id="phonecall_available"></span></tr>
                 <tr><th> Feasible Internship Period</th><th><span id="phonecall_duration"></span></tr>
              </table>
                          
              
                
                
             
<!-- Select multiple-->
           
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
   
    <section class="content col-lg-6">
     
          <div class="box box-success ">
              
            <!-- /.box-header -->
            <div class="box-body">
              
                <!-- text input -->
               <div class="form-group">
                  <label>Why do you consider yourself fit for this position?</label>
                  <p id="phonecall_question1"></p>
                </div>
                <div class="form-group">
                  <label>Have you ever worked on a project or internship before?</label>
                  <p id="phonecall_question2"></p>
                </div>
                <div class="form-group">
                  <label>What do you expect from this internship?</label>
                  <p id="phonecall_question3" ></p>
                </div>    
               
				 <div class="form-group">
                  
                  <textarea class="form-control" id="phonecall_comment"  rows="4" placeholder="Enter Your comments" style="resize:none"></textarea>
                </div>

					
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>  

   
            </div>
      
             </div>
            </div>
     
      </div>
      <div class="modal-footer" style="margin-top:-20px">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-warning" onclick="process('hold')" data-dismiss="modal">Hold</button>
        <button type="button" class="btn btn-danger" onclick="process('waitlist')" data-dismiss="modal">Waitlist</button>
        <button type="button" class="btn btn-success" onclick="process('shortlist')" data-dismiss="modal">Shortlist</button>
      </div>
    </div>
  </div>
</div>




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Telephonic Interview
        <small>All candidates who have applied</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><% out.print(Query.getCandidateCount());%></h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><%out.print(Query.getApplications(3).size()); %></h3>

              <p>Shortlisted Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-paper-airplane"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><%out.print(Query.getApplications(1).size()); %></h3>

              <p>Pending Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><%out.print(Query.getApplications(2).size()); %></h3>

              <p>Waitlisted Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-trash-a"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <div class="row">
        <div class="col-xs-12" >
        <div class="box box-warning" >
            <div class="box-header">
              <h3 class="box-title">Pending</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"  >
              <table id="pending" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody><%
                   int id=1;
                 Iterator<Application>Pending=Query.getApplications(1).listIterator();
                 
                 while(Pending.hasNext()){
                	
                	 Application current=Pending.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound1_comment()+"</td>");
                	 out.print("<td>"+"<button title=\"Make Phonecall\" type=\"button\" data-toggle=\"modal\" data-target=\"#phonecall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-phone\"></a></button>"
                			 			+"    <button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
                			 			+"</a></td>");
                	 out.print("</tr>");
                	 id++;
                 }
                 %>
                 </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Shortlisted</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
              <table id="shortlisted" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody>
                
                   <%
                   
                 Iterator<Application>Shortlisted=Query.getApplications(3).listIterator();
                 while(Shortlisted.hasNext()){
                	 Application current=Shortlisted.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound1_comment()+"</td>");
                	 out.print("<td>"+"<button title=\"Reconsider\" type=\"button\" data-toggle=\"modal\" data-target=\"#phonecall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-anchor\"></a></button>"
     			 			+"<button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
     			 			+"</a></td>");
                	 out.print("</tr>");
                	 id++;
                	
                 }
                 %>      
                </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Waitlisted</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
              <table id="waitlisted" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody>
               
                   <%
                 Iterator<Application>Waitlisted=Query.getApplications(2).listIterator();
                 while(Waitlisted.hasNext()){
                	 Application current=Waitlisted.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound1_comment()+"</td>");
                	 out.print("<td>"+"<button title=\"Reconsider\" type=\"button\" data-toggle=\"modal\" data-target=\"#phonecall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-anchor\"></a></button>"
                			 			+"    <button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
                			 			+"</a></td>");
                	 
                	 out.print("</tr>");
                	 id++;
                 }
                 %>  
                </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   </div>
  <jsp:include page="recruiter-footer.jsp"></jsp:include>
    
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<!-- datepicker -->
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>

<script>
 var internship_id,candidate;
 $('#phonecall').on('show.bs.modal', function(event) { 
	 var button = $(event.relatedTarget);
	
	document.getElementById("phonecall_username").innerHTML=document.getElementById("username_"+button.data('id')).innerHTML;
	document.getElementById("phonecall_contact").innerHTML=document.getElementById("contact_"+button.data('id')).innerHTML;
	document.getElementById("phonecall_contact").href="tel://"+document.getElementById("contact_"+button.data('id')).innerHTML;
	document.getElementById("phonecall_education").innerHTML=document.getElementById("education_"+button.data('id')).innerHTML;
	document.getElementById("phonecall_university").innerHTML=document.getElementById("university_"+button.data('id')).innerHTML;
	document.getElementById("phonecall_comment").innerHTML=document.getElementById("comment_"+button.data('id')).innerHTML;
	internship_id=button.data('internship');
	candidate=button.data('candidate');
	
	 var xmlhttp=new XMLHttpRequest();

	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	      	
	var detail=JSON.parse(xmlhttp.responseText);
	document.getElementById("phonecall_comment").value=detail.round1_comment;
	document.getElementById("phonecall_email").innerHTML=detail.email;
	document.getElementById("phonecall_prof_c").innerHTML=detail.prof_c;
	document.getElementById("phonecall_prof_java").innerHTML=detail.prof_java;
	document.getElementById("phonecall_prof_ee").innerHTML=detail.prof_ee;
	document.getElementById("phonecall_prof_js").innerHTML=detail.prof_js;
	document.getElementById("phonecall_prof_html").innerHTML=detail.prof_html;
	document.getElementById("phonecall_prof_db").innerHTML=detail.prof_db;
	document.getElementById("phonecall_prof_droid").innerHTML=detail.prof_droid;
	document.getElementById("phonecall_skills").innerHTML=detail.skills;
	document.getElementById("phonecall_available").innerHTML=detail.available;
	document.getElementById("phonecall_duration").innerHTML=detail.duration;
	document.getElementById("phonecall_question1").innerHTML=detail.question1;
	document.getElementById("phonecall_question2").innerHTML=detail.question2;
	document.getElementById("phonecall_question3").innerHTML=detail.question3;
	document.getElementById("phonecall_resume").href="uploads/"+candidate+".pdf";
	        }	
	    }
	    xmlhttp.open("POST","RetrieveApplication",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		
		xmlhttp.send("round=1"+"&applicant="+candidate+"&internship_id="+internship_id);
	 

	    
 });
 
 function process(action){
	 
		 
		 var xmlhttp=new XMLHttpRequest();

		    xmlhttp.onreadystatechange=function() {
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		      	window.location.reload();
		   
		        }	
		    }
		    xmlhttp.open("POST","ProcessApplication",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("round=1&action="+action+"&comment="+document.getElementById('phonecall_comment').value+"&internship_id="+internship_id+"&applicant="+candidate);
		 
 
 }
 
 $('#phonecall').on('hide.bs.modal', function() { 
	internship_id=0;   
	candidate="";
	
	});
 
  $(function () {
	 
	  
    $("#pending").DataTable({
    	"scrollX": true,
        
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
    $("#shortlisted").DataTable({
    	"scrollX": true,
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
    $("#waitlisted").DataTable({
    	"scrollX": true,
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
   
  } );
  
  
  </script>
</body>
</html>
