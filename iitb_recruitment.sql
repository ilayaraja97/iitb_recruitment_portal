-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2015 at 06:24 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iitb_recruitment`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `applyTechnicalInternship`(IN `internship_id` INT, IN `applicant` TEXT, IN `prof_c` TEXT, IN `prof_java` TEXT, IN `prof_ee` TEXT, IN `prof_js` TEXT, IN `prof_html` TEXT, IN `prof_db` TEXT, IN `prof_droid` TEXT, IN `skills` TEXT, IN `feasible_start` DATE, IN `feasible_end` DATE, IN `available` TEXT, IN `quest_1` TEXT, IN `quest_2` TEXT, IN `quest_3` TEXT)
    NO SQL
INSERT INTO `technical_application`( `internship_id`, `applicant`, `prof_c`, `prof_java`, `prof_ee`, `prof_js`, `prof_html`, `prof_db`, `prof_droid`, `skills`, `feasible_start`, `feasible_end`, `available`, `quest_1`, `quest_2`, `quest_3`) VALUES (internship_id, applicant, prof_c, prof_java, prof_ee, prof_js, prof_html, prof_db, prof_droid, skills, feasible_start, feasible_end, available, quest_1, quest_2, quest_3)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getApplicationDetail`(IN `applicant` TEXT, IN `internship_id` INT)
    NO SQL
Select * from technical_application inner join candidate_resume on `technical_application`.`applicant`=`candidate_resume`.`email` where `technical_application`.`applicant`=applicant and `technical_application`.`internship_id`=internship_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getApplications`(IN `status` INT)
    NO SQL
Select * from technical_application inner join candidate_resume on `technical_application`.`applicant`=`candidate_resume`.`email` where `technical_application`.`status`=status$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCandidateCount`()
    NO SQL
Select count(*) as `count` from `candidate_profile`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getInternshipDetail`(IN `internship_id` INT)
    NO SQL
SELECT  `title`, `category`, `opening`, `closing`, `duration`, `status`, `applicants`, `seats`, `hired`, `about`, `eligibility`, `information` FROM `internship` WHERE id=internship_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getInternships`()
    NO SQL
SELECT `id`, `title`, `category`, `opening`, `closing`, `duration`, `status`, `applicants`, `seats`, `hired` FROM `internship` WHERE 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getPanelists`()
    NO SQL
Select email,name,role from recruiter_profile where role='panelist'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProfile`(IN `user` TEXT)
    NO SQL
BEGIN
DECLARE type TEXT DEFAULT '';
SELECT usertype INTO type FROM `users` where `email`=user ;
IF (type='recruiter') THEN
Select * from recruiter_profile where `email`=user;
END IF;
IF (type='candidate') THEN
Select * from candidate_profile where `email`=user;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getResume`(IN `username` TEXT)
    NO SQL
Select * FROM `candidate_resume` where email=username$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getTechnicalApplication`(IN `internship_id` INT, IN `applicant` TEXT)
    NO SQL
Select * from technical_application where `technical_application`.`internship_id`=internship_id and `technical_application`.`applicant`=applicant$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `holdApplication`(IN `round` INT, IN `username` VARCHAR(40), IN `internship_id` INT, IN `comment` TEXT)
    NO SQL
if(round=1)THEN
UPDATE `technical_application` SET `technical_application`.`round1_comment`=comment,
`technical_application`.`status`=1 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
ELSEIF(round=2)THEN
UPDATE `technical_application` SET `technical_application`.`round2_comment`=comment,  `technical_application`.`status`=3 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
ELSEIF(round=3)THEN
UPDATE `technical_application` SET `technical_application`.`round3_comment`=comment,
`technical_application`.`status`=5 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
end IF$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `register`(IN `user` VARCHAR(30), IN `pass` TEXT, IN `type` TEXT, IN `name` TEXT, IN `vercode` TEXT)
    NO SQL
BEGIN
INSERT INTO `users` (`email`, `password`, `usertype`,`name`,`vercode`) VALUES (user, pass, type,name,vercode);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `shortlistApplication`(IN `round` INT, IN `username` TEXT, IN `internship_id` INT, IN `comment` TEXT)
    NO SQL
if(round=1)THEN
UPDATE `technical_application` SET `technical_application`.`round1_comment`=comment,`technical_application`.`status`=3 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
ELSEIF(round=2)THEN
UPDATE `technical_application` SET `technical_application`.`round2_comment`=comment,`technical_application`.`status`=5 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
ELSEIF(round=3)THEN
UPDATE `technical_application` SET `technical_application`.`round3_comment`=comment,`technical_application`.`status`=7 where `technical_application`.`applicant`=username and `technical_application`.`internship_id`=internship_id;
end IF$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateInternshipDetail`(IN `internship_id` INT, IN `title` TEXT, IN `category` TEXT, IN `opening` DATE, IN `closing` DATE, IN `duration` INT, IN `seats` INT, IN `about` TEXT, IN `eligibility` TEXT, IN `information` TEXT)
    NO SQL
BEGIN
IF internship_id=0 THEN SET internship_id=getNewInternship();END IF;
UPDATE `internship` SET `title`=title,`category`=category,`opening`=opening,`closing`=closing,`duration`=duration,`seats`=seats,`about`=about,`eligibility`=eligibility,`information`=information WHERE id=internship_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateResume`(IN `username` TEXT, IN `f_name` TEXT, IN `l_name` TEXT, IN `contact` TEXT, IN `location` TEXT, IN `interests` TEXT, IN `summary` TEXT, IN `course` TEXT, IN `courseyear` TEXT, IN `stream` TEXT, IN `university` TEXT, IN `grade` TEXT)
    NO SQL
UPDATE `candidate_resume` SET `first_name`=f_name,`last_name`=l_name,`contact`=contact,`location`=location,`interests`=interests,`summary`=summary,`course`=course,`courseyear`=courseyear,`stream`=stream,`university`=university,`grade`=grade WHERE `email`=username$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateTechnicalScreeningScore`(IN `username` VARCHAR(40), IN `internship_id` INT, IN `algo_easy` INT, IN `algo_medium` INT, IN `algo_hard` INT, IN `ds_easy` INT, IN `ds_medium` INT, IN `ds_hard` INT, IN `java_basics` INT, IN `java_web` INT, IN `java_oop` INT, IN `droid_basics` INT, IN `droid_projects` INT, IN `understanding` INT, IN `reasoning` INT, IN `communication` INT, IN `score` INT)
    NO SQL
UPDATE `technical_application` SET `algo_easy`=algo_easy,`algo_medium`=algo_medium,`algo_hard`=algo_hard,`ds_easy`=ds_easy,`ds_medium`=ds_medium,`ds_hard`=ds_hard,`java_basics`=java_basics,`java_web`=java_web,`java_oop`=java_oop,`droid_basics`=droid_basics,`droid_projects`=droid_projects,`understanding`=understanding,`reasoning`=reasoning,`communication`=communication ,`round2_score`=score  WHERE `internship_id`=internship_id and `technical_application`.`applicant`=username$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `waitlistApplication`(IN `round` INT, IN `username` TEXT, IN `internship_id` INT, IN `comment` TEXT)
    NO SQL
if(round=1)THEN
UPDATE `technical_application` SET `technical_application`.`round1_comment`=comment,
`technical_application`.`status`=2 where `technical_application`.`applicant`=username
and`technical_application`.`internship_id`=internship_id;
ELSEIF(round=2)THEN
UPDATE `technical_application` SET `technical_application`.`round2_comment`=comment,
`technical_application`.`status`=4 where `technical_application`.`applicant`=username and
`technical_application`.`internship_id`=internship_id;
ELSEIF(round=3)THEN
UPDATE `technical_application` SET `technical_application`.`round3_comment`=comment,
`technical_application`.`status`=6 where `technical_application`.`applicant`=username and 
`technical_application`.`internship_id`=internship_id;
end IF$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `authenticate`(`user` TEXT, `pass` TEXT) RETURNS text CHARSET latin1
    NO SQL
BEGIN
DECLARE verified_state TEXT DEFAULT '';
DECLARE type TEXT DEFAULT '';
SELECT  `verified` INTO verified_state FROM `users` where `email`=user and password=pass  ;
SELECT  `usertype` INTO type FROM `users` where `email`=user and password=pass ;
IF verified_state=1 then 

return type;
ELSEIF verified_state='' THEN 
return '';

ELSE
return concat('unverified',type);
END IF;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `getNewInternship`(`title` TEXT, `category` TEXT, `opening` DATETIME, `closing` DATETIME, `duration` INT, `seats` INT, `about` TEXT, `eligibility` TEXT, `information` TEXT) RETURNS int(11)
    NO SQL
BEGIN
DECLARE new_id INT DEFAULT '0';
INSERT INTO `internship`( `title`, `category`, `opening`, `closing`, `duration`,  `seats`,  `about`, `eligibility`, `information`) VALUES (title,category,opening,closing,duration,seats,about,eligibility,information);
SELECT LAST_INSERT_ID() INTO new_id;
RETURN new_id;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `verify`(`username` VARCHAR(40), `ver` TEXT) RETURNS tinyint(1)
    NO SQL
BEGIN
DECLARE exist INT DEFAULT 0;
DECLARE type TEXT DEFAULT '';
DECLARE name TEXT DEFAULT '';
SELECT count(*) INTO exist FROM `users` where `email`=username and `vercode`=ver ;
IF exist=1 THEN
UPDATE `users` SET `verified` = '1' WHERE `users`.`email` = username;
Select `users`.`usertype` INTO type from `users` where `email`=username;
Select `users`.`name` INTO name from `users` where `email`=username;
IF (type='recruiter') THEN INSERT INTO `recruiter_profile` (`email`,`name`) VALUES (username,name);
END IF;
IF (type='candidate')THEN INSERT INTO `candidate_profile` (`email`,`name`,`role`) VALUES (username,name,'applicant');
INSERT INTO `candidate_resume` (`email`) VALUES (username);
END IF;

return TRUE;
ELSE
return FALSE;
END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_profile`
--

CREATE TABLE IF NOT EXISTS `candidate_profile` (
  `email` varchar(40) NOT NULL,
  `name` text NOT NULL,
  `role` text NOT NULL,
  `joining_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_profile`
--

INSERT INTO `candidate_profile` (`email`, `name`, `role`, `joining_date`) VALUES
('jacob@gmail.com', 'Jacob Henry', 'applicant', '2015-12-19 15:55:59'),
('pintojoey@gmail.com', 'Joey Pinto', 'applicant', '2015-12-11 22:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_resume`
--

CREATE TABLE IF NOT EXISTS `candidate_resume` (
  `email` varchar(40) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `contact` text NOT NULL,
  `location` text NOT NULL,
  `interests` text NOT NULL,
  `summary` text NOT NULL,
  `course` text NOT NULL,
  `courseyear` text NOT NULL,
  `stream` text NOT NULL,
  `university` text NOT NULL,
  `grade` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_resume`
--

INSERT INTO `candidate_resume` (`email`, `first_name`, `last_name`, `contact`, `location`, `interests`, `summary`, `course`, `courseyear`, `stream`, `university`, `grade`) VALUES
('jacob@gmail.com', 'Jacob', 'Henry', '9561810254', 'Jaipu', 'Guitar,  Music', 'I am awesome', 'B.Tech', 'Third Year', 'Computer Science', 'IITK', '9.2'),
('pintojoey@gmail.com', 'Joey', 'Pinto', '8385006105', 'Mumbai', 'Machine Learning, Software Engineering, Image Processing, Neural Networks', 'I am a computer enthusiast and a software design specialist', 'M.Tech', 'Third Year', 'Computer Science', 'IIIT Kota', '8.77');

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE IF NOT EXISTS `internship` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `category` text NOT NULL,
  `opening` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closing` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` int(11) NOT NULL,
  `status` text NOT NULL,
  `applicants` int(11) NOT NULL,
  `seats` int(11) NOT NULL,
  `hired` int(11) NOT NULL,
  `about` text NOT NULL,
  `eligibility` text NOT NULL,
  `information` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`id`, `title`, `category`, `opening`, `closing`, `duration`, `status`, `applicants`, `seats`, `hired`, `about`, `eligibility`, `information`) VALUES
(1, 'Recruitment Portal', 'Technical', '2015-12-01 00:00:00', '2015-12-16 00:00:00', 2, 'open', 5, 1, 0, '<p>m</p>\n', '<p>Eligibility</p>\n', '<p>Information</p>\n'),
(3, 'Recruitment Por', 'Non-Technical', '2015-12-07 00:00:00', '2015-12-16 00:00:00', 2, 'open', 5, 6, 0, '<p>j</p>\n', 'eli', '<p>The</p>'),
(8, 'Test', 'Technical', '2015-12-09 00:00:00', '2015-12-31 00:00:00', 2, 'not open', 0, 2, 0, '<p>rga</p>\n', '<p>afehbg</p>\n', '<p>afb</p>\n'),
(9, 'Te', 'Technical', '2015-12-17 00:00:00', '2015-12-22 00:00:00', 1, 'not open', 0, 3, 0, '<p>EfD</p>\n', '<p>dv</p>\n', '<p>WVDS</p>\n'),
(10, 'Te', 'Technical', '2015-12-17 00:00:00', '2015-12-22 00:00:00', 1, 'not open', 0, 3, 1, '<p>EfD</p>\n', '<p>dv</p>\n', '<p>WVDS</p>\n'),
(11, 'Te', 'Technical', '2015-12-17 00:00:00', '2015-12-22 00:00:00', 1, 'not open', 0, 3, 0, '<p>EfD</p>\n', '<p>dv</p>\n', '<p>WVDS</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `recruiter_profile`
--

CREATE TABLE IF NOT EXISTS `recruiter_profile` (
  `email` varchar(40) NOT NULL,
  `name` text NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'panelist',
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiter_profile`
--

INSERT INTO `recruiter_profile` (`email`, `name`, `role`, `joining_date`) VALUES
('alps.j07@gmail.com', 'Alpana Jamadagni', 'panelist', '2015-12-22 10:59:39'),
('martin@gmail.com', 'Frank Martin', 'panelist', '2015-12-19 10:24:03'),
('pintojoe@gmail.com', 'Joe Pinto', 'panelist', '2015-12-11 09:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `technical_application`
--

CREATE TABLE IF NOT EXISTS `technical_application` (
  `internship_id` int(11) NOT NULL,
  `applicant` varchar(40) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `prof_c` text NOT NULL,
  `prof_java` text NOT NULL,
  `prof_ee` text NOT NULL,
  `prof_js` text NOT NULL,
  `prof_html` text NOT NULL,
  `prof_db` text NOT NULL,
  `prof_droid` text NOT NULL,
  `skills` text NOT NULL,
  `feasible_start` date NOT NULL,
  `feasible_end` date NOT NULL,
  `available` text NOT NULL,
  `quest_1` text NOT NULL,
  `quest_2` text NOT NULL,
  `quest_3` text NOT NULL,
  `round1_comment` text NOT NULL,
  `round2_date` datetime DEFAULT NULL,
  `round3_date` datetime DEFAULT NULL,
  `round2_comment` text NOT NULL,
  `round2_score` text NOT NULL,
  `round2_panelist` text NOT NULL,
  `round3_comment` text NOT NULL,
  `round3_score` text NOT NULL,
  `round3_panelist` text NOT NULL,
  `algo_easy` int(11) NOT NULL,
  `algo_medium` int(11) NOT NULL,
  `algo_hard` int(11) NOT NULL,
  `ds_easy` int(11) NOT NULL,
  `ds_medium` int(11) NOT NULL,
  `ds_hard` int(11) NOT NULL,
  `java_basics` int(11) NOT NULL,
  `java_web` int(11) NOT NULL,
  `java_oop` int(11) NOT NULL,
  `droid_basics` int(11) NOT NULL,
  `droid_projects` int(11) NOT NULL,
  `understanding` int(11) NOT NULL,
  `reasoning` int(11) NOT NULL,
  `communication` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `technical_application`
--

INSERT INTO `technical_application` (`internship_id`, `applicant`, `status`, `prof_c`, `prof_java`, `prof_ee`, `prof_js`, `prof_html`, `prof_db`, `prof_droid`, `skills`, `feasible_start`, `feasible_end`, `available`, `quest_1`, `quest_2`, `quest_3`, `round1_comment`, `round2_date`, `round3_date`, `round2_comment`, `round2_score`, `round2_panelist`, `round3_comment`, `round3_score`, `round3_panelist`, `algo_easy`, `algo_medium`, `algo_hard`, `ds_easy`, `ds_medium`, `ds_hard`, `java_basics`, `java_web`, `java_oop`, `droid_basics`, `droid_projects`, `understanding`, `reasoning`, `communication`) VALUES
(8, 'pintojoey@gmail.com', 3, 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'undefined', 'Shell Scripting', '2015-12-19', '2015-12-25', 'undefined', '11', '22', '33', 'DID NOT', '2015-12-17 08:34:21', '2015-12-30 11:13:46', 'round2_comment made', '17', 'Sanket', '', '', '', 0, 0, 0, 0, 0, 0, 3, 0, 0, 2, 2, 2, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `email` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `usertype` text NOT NULL,
  `name` text NOT NULL,
  `vercode` text NOT NULL,
  `changepass` text NOT NULL,
  `verified` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `password`, `usertype`, `name`, `vercode`, `changepass`, `verified`) VALUES
('alps.j07@gmail.com', 'e19d5cd5af0378da05f63f891c7467af', 'recruiter', 'Alpana Jamadagni', 'JnQXycgeAXPp', '', 1),
('jacob@gmail.com', '47c9ed467bc399b71d380b4c2110364e', 'candidate', 'Jacob Henry', 'nUUulfcN6HQX', '', 1),
('martin@gmail.com', '47c9ed467bc399b71d380b4c2110364e', 'recruiter', 'Frank Martin', 'nIDddgYMqUkM', '', 1),
('pintojoe@gmail.com', '47c9ed467bc399b71d380b4c2110364e', 'recruiter', 'Joey Pinto', 'PzbGyKa4CP3X', '', 1),
('pintojoey@gmail.com', '47c9ed467bc399b71d380b4c2110364e', 'candidate', 'Joey Pinto', 'Z8VU6dFHyiVP', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidate_profile`
--
ALTER TABLE `candidate_profile`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `candidate_resume`
--
ALTER TABLE `candidate_resume`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruiter_profile`
--
ALTER TABLE `recruiter_profile`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `technical_application`
--
ALTER TABLE `technical_application`
  ADD PRIMARY KEY (`internship_id`,`applicant`), ADD UNIQUE KEY `internship_id` (`internship_id`,`applicant`), ADD UNIQUE KEY `internship_id_2` (`internship_id`,`applicant`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `internship`
--
ALTER TABLE `internship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
